// Setup
var sum = 0;

function addThree() {
  sum = sum + 3;
}

// Only change code below this line
function addFive(a) {
  sum = sum + 5;
}

// Only change code above this line

addThree();
addFive();
